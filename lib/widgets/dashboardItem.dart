import 'package:flutter/material.dart';

class DashboardItem extends StatelessWidget {
  final String label;
  final IconData icon;
  const DashboardItem({
    Key? key,
    required this.icon,
    required this.label,

  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
     // height: size.height,
      child: Card(
        color: Colors.green,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(icon,color: Colors.white,size: 50,),
            Text(label,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)
          ],
        ),
      ),
    );
  }
}