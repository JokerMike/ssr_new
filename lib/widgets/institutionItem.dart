import 'package:flutter/material.dart';
class InstitutItem extends StatelessWidget {
  final String title;
  final String subtitle;
  final IconData icon;
 
  const InstitutItem({
    Key? key,  required this.title,required this.subtitle,required this.icon
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: Container(
            height: 40,
            width: 40,
            decoration: BoxDecoration(
              color: Colors.white
            ),
            child: Icon(icon,color: Colors.green,),
          ),
          title: Text(title,style: TextStyle(color: Colors.white,fontSize: 20),),
          subtitle: Text(subtitle),
          trailing: Icon(Icons.arrow_forward_ios,color: Colors.white,),
        ),
        Padding(
          padding: const EdgeInsets.only(left:15.0,right: 18.0),
          child: Divider(color: Colors.white,thickness: 1.0,),
        )
      ],
    );
  }
}
