import 'package:flutter/material.dart';
import 'package:last_design/ui/Home.dart' as screens;
import 'package:last_design/ui/Login.dart' as screens;
import 'package:last_design/ui/Spash.dart';
import 'package:provider/provider.dart';
import 'providers/User.dart' as providers;
import 'ui/Notification.dart' as screens;
import 'ui/Observation.dart' as screens;
import 'ui/SearchInfo.dart' as screens;
import 'package:ably_flutter/ably_flutter.dart' as ably;

import 'package:uuid/uuid.dart';
class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late ably.Realtime realtimeInstance;
  var newMsgFromAbly;
 late ably.RealtimeChannel chatChannel;
  var myRandomClientId = '';


  void createAblyRealtimeInstance() async {
    var uuid = Uuid();
    myRandomClientId = uuid.v4();
    var clientOptions =
        ably.ClientOptions.fromKey("hf8FDg.EgVeqw:c6e5zZAau4rLutFt");
    clientOptions.clientId = myRandomClientId;
    try {
      realtimeInstance = ably.Realtime(options: clientOptions);
      print('Ably instantiated');
      chatChannel = realtimeInstance.channels.get('testchannel');
      subscribeToChatChannel();
      realtimeInstance.connection
          .on(ably.ConnectionEvent.connected)
          .listen((ably.ConnectionStateChange stateChange) async {
        print('Realtime connection state changed: ${stateChange.event}');
      });
    } catch (error) {
      print('Error creating Ably Realtime Instance: $error');
      rethrow;
    }
  }
   void subscribeToChatChannel() {
    
    var messageStream = chatChannel.subscribe();
    messageStream.listen((ably.Message message) {
      newMsgFromAbly = message.data;
      print(newMsgFromAbly);


     }
     
     
    );

  }

   @override
      void initState() { 
        createAblyRealtimeInstance();
        super.initState();
        
      }
  @override
  Widget build(BuildContext context) {
    // var user= Provider.of<User>(context);
    //user.initInfo();
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => providers.User(),
          ),
        ],
        child: Consumer<providers.User>(builder: (context, auth, child) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'SSR',
            theme: ThemeData(primarySwatch: Colors.green, fontFamily: 'Khula'),
            home: Splash(),
            //user.isToken? Text("data"):Text("toto"),
            routes: {
                screens.Home.routeName: (context) => screens.Home(),
                screens.Observation.routeName: (context) => screens.Observation(),
                screens.Login.routeName: (context) => screens.Login(),
                screens.InfoEngine.routeName: (context) => screens.InfoEngine(),
                screens.Notifications.routeName: (context) => screens.Notifications(),
            },
          );
        }));
  }
}
