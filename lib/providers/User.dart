//import 'package:ikmoney/src/resources/shared_preferences.dart' as resources;
import 'package:flutter/widgets.dart';
import 'package:last_design/ressources/api.dart';
import 'package:last_design/ressources/shared_preferences.dart' as resources;

class User with ChangeNotifier{
  late String nom;
  late String grade;
  late String arrondissement;
  late String institution;
  late String token;

bool get isToken{
  return token != null;
}
  String get getName {
    return nom;
  }
  String get getGrade {
    return grade;
  }
  String get getArr{
    return arrondissement;
  }
  String get getIns{
    return institution;
  }
  
  String get getToken{
    return token;
  }

  initInfo() async{
     await resources.SharedPreferencesClass.restore("nom").then((value) {
      if (value is String) {
        nom = value;
      }
    });
    await resources.SharedPreferencesClass.restore("grade").then((value) {
      if (value is String) {
        grade = value;
      }
    });
    await resources.SharedPreferencesClass.restore("arrondissement").then((value) {
      if (value is String) {
        arrondissement = value;
      }
    });
    await resources.SharedPreferencesClass.restore("institution").then((value) {
      if (value is String) {
        institution = value;
      }
    });
  }

  Future<String>login(String username,String password)async{
    try{
        final response = await Api.login(username, password);
      print(response);
      String validate = response["status"];

      if (validate.toLowerCase() == "success")
      {
         final data = response["data"];
         await resources.SharedPreferencesClass.save(
          "nom",
          nom,
        );
        await resources.SharedPreferencesClass.save(
          "token",
          token,
        );
        await resources.SharedPreferencesClass.save(
          "grade",
          grade,
        );
        await resources.SharedPreferencesClass.save(
          "arrondissement",
          arrondissement,
        );
         await resources.SharedPreferencesClass.save(
          "institution",
          institution,
        );
         notifyListeners();
        return "success";
      }else if (validate.toLowerCase() == "error") {
        notifyListeners();
        return  "error";
      } else {
        notifyListeners();
        return "error";
      }
    }catch (err) {
      throw err;
    }

  }

}