
import 'package:flutter/material.dart';
import 'package:last_design/ressources/constantes.dart';

enum Vehicule { moto, tricycle, Voiture }

class Observation extends StatefulWidget {
  static const routeName = "/observation";
  const Observation({Key? key}) : super(key: key);

  @override
  _ObservationState createState() => _ObservationState();
}
enum Engin { moto, tricycle, voiture}
class _ObservationState extends State<Observation> { 
  Engin item = Engin.moto;

  late String value;
  int default_index = 1;
  String motif="infraction";
/*   getSim(int length) {
    List<Widget> list = <Widget>[];

    list.add(Row(
        children: [
          Text("Sim ${i + 1}"),
          Radio(
              value: i + 1,
              groupValue: default_index,
              onChanged: (value) {
                setState(() {
                  default_index = i + 1;
                 
                });
              }),
        ],
      ));
    return Row(children: list);
  }  */
  Vehicule? _character = Vehicule.moto;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green.shade400,
        elevation: 0,
      ),
      body:  ListView(children: [
              Container(
                height: 150,
                width: double.infinity,
                color: Colors.green.shade400,
                child: ListView(
                 /*  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center, */
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        bottom: 0,
                      ),
                      child: Center(
                        child: Text(
                          "Observation".toUpperCase(),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: (){
                              setState(() {
                                motif="vol";
                              });
                            },
                            child: Container(
                              height: 50,
                              width: 80,
                              
                              child: Card(
                                color: motif=="vol"?Colors.red:Colors.white,
                                child: Center(child: Text("VOL",style: TextStyle(
                                    color: motif=="vol"?Colors.white:Colors.black
                                ),)),
                              ),
                            ),
                          ),
                          SizedBox(width: 20,),
                           GestureDetector(
                              onTap: (){
                              setState(() {
                                motif="infraction";
                              });
                            },
                             child: Container(
                              height: 50,
                              width: 120,
                              child: Card(
                                 color: motif=="infraction"?Colors.red:Colors.white,
                                child: Center(child: Text("INFRACTION",style: TextStyle(
                                  color: motif=="infraction"?Colors.white:Colors.black
                                ),)),
                              ),
                                                           ),
                           )
                        ],
                      ),
                      SizedBox(height: 0,)
                  ],
                ),
              ),

              Visibility(
                  visible: true,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 5.0, left: 15, right: 15),
                    child: Form(
                      child: Column(
                        children: [
                          Column(
        children: <Widget>[
          ListTile(
            title: const Text('Moto'),
            leading: Radio<Engin>(
              value: Engin.moto,
              groupValue: item,
              onChanged: (value) {
                setState(() {
                  item= value!;
                });
              },
            ),
          ),
          ListTile(
            title: const Text('Tricycle'),
            leading: Radio<Engin>(
              value: Engin.tricycle,
              groupValue: item,
              onChanged: (value) {
                setState(() {
                  item= value!;
                });
              },
            ),
          ),
          ListTile(
            title: const Text('Voiture'),
            leading: Radio<Engin>(
              value: Engin.voiture,
              groupValue: item,
              onChanged: (value) {
                setState(() {
                  item= value!;
                });
              },
            ),
          ),
    
        ],
      ),
                          TextFormField(
                            textCapitalization: TextCapitalization.characters,
                            decoration: InputDecoration(
                                helperText: "Nom du proprietaire"),
                          ),
                           TextFormField(
                           // textCapitalization: TextCapitalization.characters,
                            decoration: InputDecoration(
                                helperText: "Prenom du proprietaire"),
                          ),
                           TextFormField(
                             maxLength: 10,
                              keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                helperText: "Numero de permis"),
                          ),
                           TextFormField(
                             maxLength: 2,
                               textCapitalization: TextCapitalization.characters,
                            decoration: InputDecoration(
                                helperText: "Serie"),
                          ),
                          TextFormField(
                            maxLength: 4,
                              keyboardType: TextInputType.number,
                            decoration: InputDecoration(
                                helperText: "Numero de plaque"),
                          ),
                          ElevatedButton(
                              onPressed: () {},
                              child: Text(
                                "Enregistrer l'observation",
                                style: TextStyle(fontSize: 20),
                              ))
                        ],
                      ),
                    ),
                  ))
            ])
      
    );
  }
}
