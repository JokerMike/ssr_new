import 'dart:ui';
import 'package:flutter/material.dart';
class Notifications extends StatelessWidget {
  static const routeName = "/notifications";
  const Notifications({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.red,
        title: Text("Notifications"),
        centerTitle: true,
      ),
    body: ListView(
      children: [
        Padding(
          padding: const EdgeInsets.only(top:0.0,bottom: 15.0),
          child: GestureDetector(
            onTap: (){
                showDialog<void>(
                    context: context,
                    barrierDismissible: true, // user must tap button!
                    builder: (BuildContext context) {
                      return SimpleDialog(
                        title: Center(child: Text("Information",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),)),
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left :15.0,right: 15,top: 5,bottom: 5),
                            child: Text("dkslgnfslnavkdsn;ajkndsvjka;njkfnanvsd;kjnvcjaknbsfcxjnakvnjkfnbjkvhdjkfanhjfkdanbjkdfsnhbjfkvcnkbavn;nbjkf<;njfkdnhajknha;skjfakfakhjalkjhkadhjkashjk",style: TextStyle(fontSize: 18),),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(28.0),
                            child: ElevatedButton(onPressed: (){
                            Navigator.pop(context);
                            }, child: Text("bien recu"),),
                          )
                        ],
                      );});
            },
            child: Container(
              height: 70,
              decoration: BoxDecoration(
                color: Colors.yellow.withOpacity(0.3)
              ),
              child: Center(
                child: ListTile(
                  leading: Icon(Icons.info_rounded,size: 40,color: Colors.red,),
                title: Text("Objet",style: TextStyle(fontWeight: FontWeight.bold),),
                subtitle:Text("Recherche de vehicule disparu",style: TextStyle()) ,
                trailing: Icon(Icons.arrow_forward_ios_rounded),
                ),
              ),
            ),
          ),
        )
      ],
    ),
    );
  }
}
