import 'package:flutter/material.dart';
import 'package:last_design/ressources/constantes.dart';
import 'package:last_design/ui/Home.dart' as screens;
import 'package:last_design/ui/Login.dart';

import 'Home.dart';
class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Init.initialize(),
      builder: (context, AsyncSnapshot snapshot) {
        // Show splash screen while waiting for app resources to load:
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Scaffold(
      backgroundColor: Colors.white,
      body: Center(child: Image.asset(pathImage+"togo_fat.png")),
    );
        } else {
          // Loading is done, return the app:
          return Home();
        }
      },
    );
  }
 
}

class Init {


 static Future initialize() async {
    // This is where you can initialize the resources needed by your app while
    // the splash screen is displayed.  Remove the following example because
    // delaying the user experience is a bad design practice!
    await Future.delayed(Duration(seconds: 3));
  }
}
