import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:last_design/ressources/constantes.dart';
import 'package:last_design/widgets/dashboardItem.dart';
import 'package:last_design/widgets/institutionItem.dart';

import 'Notification.dart' as screens;
import 'Observation.dart' as screens;

class Home extends StatelessWidget {
  static const routeName = "/home";

  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 5,
          ),
          child: FittedBox(
            child: Text(
              "Republique Togolaise",
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 2.0),
            ),
          ),
        ),
        centerTitle: false,
        backgroundColor: Colors.green.shade400.withOpacity(0.9),
        elevation: 0,
        leading: Padding(
          padding: const EdgeInsets.only(
            top: 2,
            bottom: 2,
            right: 2,
            left: 15,
          ),
          child: Container(
            child: Image.asset(
              pathImage + "togo_fat.png",
              width: 20,
            ),
          ),
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Container(
              height: size.height * 0.75,
              width: double.infinity,
              color: Colors.green.shade400.withOpacity(0.9),
              child: ListView(
                shrinkWrap: true,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Center(
                    child: Text("Police".toUpperCase(),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.bold)),
                  ),
                  SizedBox(
                    height: 05,
                  ),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushNamed(screens.Observation.routeName);
                    },
                    child: InstitutItem(
                     
                      icon: Icons.ac_unit,
                      subtitle: 'Creer une observation',
                      title: 'Observation',
                    ),
                  ),
                  GestureDetector(
                    onTap: (){
                      showDialog<void>(
                    context: context,
                    barrierDismissible: true, // user must tap button!
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Center(
                            child: Text(
                          'Informations',
                          style: TextStyle(fontSize: 30),
                        )),
                        content: SingleChildScrollView(
                          child: ListBody(
                            children: <Widget>[
                              TextFormField(
                               // controller: password,
                               textCapitalization: TextCapitalization.characters,
                               maxLength: 2,
                                textAlign: TextAlign.justify,
                                decoration: InputDecoration(
                                  fillColor: Colors.red,
                                  hintText: "Serie du vehicule",
                                  hintStyle: TextStyle(color: Colors.black),
                                ),
                              ),
                              TextFormField(
                               // controller: password,
                                textAlign: TextAlign.justify,
                               maxLength: 4,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  fillColor: Colors.red,
                                  hintText: "Numero de la plaque",
                                  hintStyle: TextStyle(color: Colors.black),
                                ),
                              ),
                              TextFormField(
                               // controller: password,
                                textAlign: TextAlign.justify,
                               initialValue: "TOGO",
                               textCapitalization: TextCapitalization.characters,
                                decoration: InputDecoration(
                                  fillColor: Colors.red,
                                  hintText: "Pays du vehicule",
                                  hintStyle: TextStyle(color: Colors.black),
                                ),
                              ),
                              SizedBox(height: 20,),
                              ElevatedButton(
                                child: Text('rechercher',style: TextStyle(fontSize: 20,color:Colors.white,)),
                                    style: ElevatedButton.styleFrom(
                              primary: Colors.green,),
                                onPressed: () {
                                  Navigator.pop(context);
                                   Navigator.of(context).pushNamed("/recherche");
                                },
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  );
                    },
                    child: InstitutItem(
                     
                      icon: Icons.car_rental,
                      subtitle: "verifier une plaque de vehicule",
                      title: "Verification",
                    ),
                  ),
                  InstitutItem(
                    icon: Icons.warning,
                    subtitle: "Consulter mes alertes",
                    title: "Alertes",
                  ),
                  InstitutItem(
                    icon: Icons.car_repair_outlined,
                    subtitle: "Verifier l'autorisation de vitre teinte",
                    title: "Vitres",
                  ),
                 SizedBox(height: 150,)
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(
                  left: 20.0, right: 20.0, bottom: 15.0, top: 15.0),
              child: Container(
                color: Colors.white,
                height: size.height * 0.30,
                child: GridView.count(
                    crossAxisCount: 2,
                    childAspectRatio: (3 / 2),
                    crossAxisSpacing: 1,
                    mainAxisSpacing: 1,
                    //  physics:BouncingScrollPhysics(),
                    padding: EdgeInsets.all(3.0),
                    children: [
                      DashboardItem(
                        label: "mon compte",
                        icon: Icons.person,
                      ),
                      Container(
     // height: size.height,
      child: GestureDetector(
        onTap: (){
          Navigator.of(context).pushNamed(screens.Notifications.routeName);
        },
        child: Card(
          color: Colors.green,
          child: Stack(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Icon(Icons.notifications,color: Colors.white,size: 50,),
                ),
              ),
              Positioned(
                top: 65,left: 35,
                child: Text("Notifications",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)),
      
              Positioned(
                top: 15,left: 78,
                child: CircleAvatar(backgroundColor: Colors.red,radius: 10,))
              
            ],
          ),
        ),
      ),
    ),
                      /* DashboardItem(
                          label: "Notifications", icon: Icons.notifications), */
                      DashboardItem(label: "frontieres", icon: Icons.door_back,),
                      DashboardItem(
                          label: "Deconnexion", icon: Icons.power_settings_new),
                    ]),
              ),
            ),
          )
        ],
      ),
    );
  }
}
