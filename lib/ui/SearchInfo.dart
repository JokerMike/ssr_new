import 'package:flutter/material.dart';

class InfoEngine extends StatefulWidget {
  static const routeName = '/recherche';
  @override
  _InfoEngineState createState() => _InfoEngineState();
}

class _InfoEngineState extends State<InfoEngine> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        elevation: 0,
        automaticallyImplyLeading: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          height: size.height,
          color: Colors.yellow.shade100,
          child: ListView(
            children: [
              Container(
                height: size.height /5,
                decoration: BoxDecoration(
                    color: Colors.green,
                    ),
                    child:  Center(
                child: Text(
                  "Informations",
                  style: TextStyle(
                      fontSize: 45,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ),
              ),

              Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 10.0, right: 8.0, left: 8.0),
                child: Container(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Type d'engin",
                        style:
                            TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                      ),
                      Text(
                        "Voiture",
                        style:
                            TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 15.0, right: 8.0, left: 8.0),
                child: Container(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Alerte en cours",
                        style:
                            TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                      ),
                      Text(
                        "OUI",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.red),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 15.0, right: 8.0, left: 8.0),
                child: Container(
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Motif",
                        style:
                            TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                      ),
                      Text(
                        "Infraction",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.red),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 15.0, right: 8.0, left: 8.0),
                child: Container(
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Date",
                        style:
                            TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                      ),
                      Text(
                        "27-05-2021",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 15.0, right: 8.0, left: 8.0),
                child: Container(
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Numero de permis",
                        style:
                            TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                      ),
                      Text(
                        "8795321478",
                        style:
                            TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 15.0, right: 8.0, left: 8.0),
                child: Container(
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Ville de signalement",
                        style:
                            TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                      ),
                      Text(
                        "Lome",
                        style:
                            TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 15.0, right: 8.0, left: 8.0),
                child: Container(
                  height: 20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "contact pour signaler",
                        style:
                            TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
                      ),
                      Text(
                        "98969321",
                        style:
                            TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),

              
            ],
          ),
        ),
      ),
    );
  }
}
