
import 'package:flutter/material.dart';
import 'package:last_design/ressources/constantes.dart';
import 'package:last_design/widgets/widgets.dart';

import 'package:provider/provider.dart';


class Login extends StatefulWidget {
  static const routeName = "/login";
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final formKey = new GlobalKey<FormState>();

  late String _username;
  late String _password;

  var loading = Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      CircularProgressIndicator(),
      Text(" Authenticating ... Please wait")
    ],
  );

  Widget _image() {
    return Image.asset(pathImage+"togo_fat.png");
  }

  Widget _submitButton() {
    return InkWell(
      onTap: (){
        Navigator.of(context).pushNamed("/home");
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [Color(0xff008f39), Color(0xff008f39)])),
        child: Text(
          'Se Connecter',
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  Widget _divider() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          Text('or'),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Divider(
                thickness: 1,
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

  Widget _title() {
    return Text("SSR",style: TextStyle(fontSize: 30),);
  
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
   

    final usernameField = TextFormField(
      
      autofocus: false,
      onSaved: (value) => _username = value!,
      decoration:buildInputDecoration("Confirm password", Icons.email),
    );

    final passwordField = TextFormField(
      autofocus: false,
      obscureText: true,
      validator: (value) => value!.isEmpty ? "Please enter password" : null,
      onSaved: (value) => _password = value!,
      decoration: buildInputDecoration("Confirm password", Icons.lock),
    );

 

    return Scaffold(
        body: Container(
      decoration: BoxDecoration(color: Colors.white),
      height: height,
      child: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 30,),
                    _image(),
                    SizedBox(height: 20,),
                    _title(),
                    
                    label("Nom d'utilisateur"),
                    SizedBox(height: 5.0),
                    usernameField,
                    SizedBox(height: 20.0),
                    label("Mot de passe"),
                    SizedBox(height: 5.0),
                    passwordField,
                    SizedBox(height: 20),
                   _submitButton()
                    
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
